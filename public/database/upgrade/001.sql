CREATE TABLE FORMATION (
    id integer NOT NULL,
    nom varchar NOT NULL,
    primary key (id)
);

CREATE TABLE PROFESSEUR (
    id integer NOT NULL,
    prenom varchar NOT NULL,
    nom varchar NOT NULL,
    primary key (id)
);

CREATE TABLE SEMESTRE (
    id integer NOT NULL,
    primary key (id)
);

CREATE TABLE COMPTE (
    id varchar NOT NULL,
    nom varchar,
    prenom varchar,
    motDePasse varchar,
    idFormation integer NOT NULL,
    grade integer,
    dateCreation date NOT NULL,
    primary key (id, idFormation),
    foreign key (idFormation) references FORMATION (id)
);

CREATE TABLE COURS (
    id integer NOT NULL,
    nom varchar NOT NULL,
    idFormation integer NOT NULL,
    idSemestre integer NOT NULL,
    description varchar,
    primary key (id),
    foreign key (idFormation) references FORMATION(id),
    foreign key (idSemestre) references SEMESTRE(id)
);

CREATE TABLE ASSURER_COURS (
    idCours integer NOT NULL,
    idProf integer NOT NULL,
    primary key (idCours, idProf),
    foreign key (idCours) references COURS(id),
    foreign key (idProf) references PROFESSEUR(id)
);

CREATE TABLE FICHIER (
    id integer NOT NULL,
    idCours integer NOT NULL,
    idFormation integer NOT NULL,
    nom varchar NOT NULL,
    dateFich date NOT NULL,
    idauteur varchar NOT NULL,
    extension varchar,
    typeCours varchar NOT NULL,
    description varchar,
    primary key (id, idCours),
    foreign key (idCours) references COURS(id),
    foreign key (idAuteur, idFormation) references COMPTE(id, idFormation)
);

INSERT INTO FORMATION VALUES(1, 'DUT Informatique');

INSERT INTO PROFESSEUR VALUES(1, 'Fanny', 'BINET');
INSERT INTO PROFESSEUR VALUES(2, 'Irina', 'ILLINA');
INSERT INTO PROFESSEUR VALUES(3, 'Paul', 'CAILLON');
INSERT INTO PROFESSEUR VALUES(4, 'Victorien', 'ELVINGER');
INSERT INTO PROFESSEUR VALUES(5, 'Etienne', 'ANDRE');
INSERT INTO PROFESSEUR VALUES(6, 'Michael', 'LATHAM');
INSERT INTO PROFESSEUR VALUES(7, 'El Haj', 'LAAMRI');
INSERT INTO PROFESSEUR VALUES(8, 'Rémy', 'BOURGUIGNON');
INSERT INTO PROFESSEUR VALUES(9, 'Alain', 'LONGHAIS');
INSERT INTO PROFESSEUR VALUES(10, 'Dominique', 'COLINET');
INSERT INTO PROFESSEUR VALUES(11, 'Denis', 'ROEGEL');
INSERT INTO PROFESSEUR VALUES(12, 'Emmanuel', 'NATAF');
INSERT INTO PROFESSEUR VALUES(13, 'Amirouche', 'HADDOU');
INSERT INTO PROFESSEUR VALUES(14, 'Christelle', 'LACRESSE');
INSERT INTO PROFESSEUR VALUES(15, 'Bernard', 'MANGEOL');
INSERT INTO PROFESSEUR VALUES(16, 'Abdessamad', 'IMINE');
INSERT INTO PROFESSEUR VALUES(17, 'Pierre-André', 'GUENEGO');
INSERT INTO PROFESSEUR VALUES(18, 'Yolande', 'BELAID');
INSERT INTO PROFESSEUR VALUES(19, 'Anne-Cécile', 'SCHNEIDER');
INSERT INTO PROFESSEUR VALUES(20, 'Berengere', 'STASSIN');
INSERT INTO PROFESSEUR VALUES(21, 'Samira', 'MESSAOUDI');
INSERT INTO PROFESSEUR VALUES(22, 'Patrick', 'NOURRISSIER');
INSERT INTO PROFESSEUR VALUES(23, 'Amine', 'BOUMAZA');
INSERT INTO PROFESSEUR VALUES(24, 'Catherine', 'MARIAGE');

INSERT INTO SEMESTRE VALUES(1);
INSERT INTO SEMESTRE VALUES(2);
INSERT INTO SEMESTRE VALUES(3);
INSERT INTO SEMESTRE VALUES(4);
