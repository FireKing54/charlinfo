-- Fichier qui reset la base de données

-- Supprimer tous les données des tables
TRUNCATE TABLE Cours, Assurer_Cours, Professeur, Formation, Fichier, Compte, Semestre, Changelog, Description_changelog;

-- Supprimer les tables
DROP TABLE Cours, Assurer_Cours, Professeur, Formation, Fichier, Compte, Semestre, Changelog, Description_changelog CASCADE;
