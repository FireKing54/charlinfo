CREATE TABLE CHANGELOG (
    nom varchar NOT NULL,
    date TIMESTAMP WITH TIME ZONE NOT NULL,
    primary key (nom)
);

CREATE TABLE DESCRIPTION_CHANGELOG (
    nomChangelog varchar NOT NULL,
    description varchar NOT NULL,
    primary key (nomChangelog, description),
    foreign key (nomChangelog) references CHANGELOG(nom)
);