const fs = require('fs');

/**
 * Fonctions de permissions et de gestion des grades
 */
const PERMISSION = require("../modules/permission.js");

/**
 * Fonctions pratiques qui se chargent de simplifier et d'alléger le code
 */
const ACTION = require("../modules/action.js");

/**
 * Fonctions de gestion des dates
 */
const DATE = require("../modules/date.js");

/**
 * Fonctions nécessaires au chargement de la page
 */
const {
    requete,
    requete_recuperer_version
} = require("../modules/requete.js");
const { getSeqFromFolder, getSeqFromIntegers, getExtension, verifyIfNotInt } = require("../modules/misc.js");
const { logcolor, logwarning, logerror } = require("../modules/color.js")

/**
 * Traitement de la connexion au site
 */
exports.connexion = async function (req, res) {
    let reqe = await requete("SELECT COUNT(*) FROM Compte WHERE id = $1", [req.body.user]);
    if (reqe[0].count == 0) {
        req.session.login = "Nom d'utilisateur erroné";
        return await res.redirect('/');
    }
    let pass = await require('../../../charlinfo-db/users/hashage/hash.js').hash(req.body.pass);
    let value = await requete("SELECT COUNT(*) FROM Compte WHERE id = $1 AND motDePasse = $2", [req.body.user, pass])
    if (value[0].count == 1) {
        req.session.user = await (await requete("SELECT id, nom, prenom, grade, idformation FROM Compte WHERE id = $1", [req.body.user]))[0];
        return await res.redirect('/');
    }
    req.session.login = "Mot de passe erroné";
    await res.redirect('/');
}


/**
 * Traitement de l'inscription au site
 */
exports.inscription = async function (req, res) {
    let reqe = await requete("SELECT COUNT(*) FROM Compte WHERE id = $1", [req.body.user]);

    if (reqe[0].count == 0) {
        req.session.sub = "Votre compte n'est pas référencé";
        return await res.redirect('/');
    }
    let mdp = await require('../../../charlinfo-db/users/hashage/hash.js').hash(req.body.pass)
    reqe = await requete("SELECT COUNT(*) FROM Compte WHERE id = $1 AND motDePasse = $2", [req.body.user, mdp])
    if (reqe[0].count == 1) {
        req.session.sub = "Vous avez déjà un compte";
        return await res.redirect('/');
    }

    let prenom = req.body.prenom;
    let nom = req.body.nom

    prenom = await prenom.toLowerCase();
    let firstLetter = await prenom.charAt(0);
    prenom = await prenom.replace(firstLetter.toLowerCase(), firstLetter.toUpperCase());
    nom = await nom.toUpperCase();

    reqe = await requete("SELECT id FROM Formation WHERE nom = $1", [req.body.formation]);
    if (reqe == undefined) {
        console.log("La formation n'existe pas");
        return await res.redirect("/");
    }

    let dat = await DATE.dateISO();
    await requete("UPDATE Compte SET nom = $1 WHERE id = $2", [nom, req.body.user]);
    await requete("UPDATE Compte SET prenom = $1 WHERE id = $2", [prenom, req.body.user]);
    await requete("UPDATE Compte SET motDePasse = $1 WHERE id = $2", [mdp, req.body.user]);
    await requete("UPDATE Compte SET grade = $1 WHERE id = $2", [1, req.body.user]);
    await requete("UPDATE Compte SET idFormation = $1 WHERE id = $2", [1, req.body.user]);
    await requete("UPDATE Compte SET dateCreation = $1 WHERE id = $2", [dat, req.body.user]);

    let requett = await requete("SELECT id, nom, prenom, grade, idformation FROM Compte WHERE id = $1", [req.body.user]);
    req.session.user = await requett[0];

    ACTION.ajouterLogsUtilisateur(req.body.user);
    await res.redirect("/charte");
}


/**
 * Traitement de l'ajout d'un fichier pour un cours
 */
exports.ajoutFichierCours = async function (req, res) {

    /**
     * Taille limite d'upload de fichier (en Mo)
     */
    const TAILLE_LIMITE_UPLOAD_FICHIER = 5.0;

    await PERMISSION.gererPermission(req, res, "/", 1);

    if (req.files === undefined) {
        logerror(`Le fichier envoyé au cours ${req.params.cours} a pas réussi a être envoyé : req.files === undefined`);
        return await res.redirect(`/cours/afficher/${req.params.cours}`);
    }

    if (req.files.contenu.size > TAILLE_LIMITE_UPLOAD_FICHIER * 1000000) {
        req.session.tropGros = await `Le fichier que vous voulez envoyer est trop gros (il fait ${(Math.round((req.files.contenu.size) / 10000) / 100)}Mo alors que la limite est de ${(TAILLE_LIMITE_UPLOAD_FICHIER / 1000000)}Mo).`
        return await res.redirect(`/cours/ajoutFichier/${req.params.cours}`);
    }

    let extens = await getExtension(req.files.contenu.name);
    let val = await getSeqFromFolder(`../charlinfo-db/cours/${req.params.cours}`);
    let reqe = await requete("SELECT idFormation FROM COURS WHERE id = $1::integer", [req.params.cours])

    requete("INSERT INTO Fichier VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)", [
        val, req.params.cours, await reqe[0].idformation, req.body.titre, await DATE.dateISO(),
        req.session.user.id, extens, req.body.type, req.body.desc
    ]);
    if (extens == null) {
        fs.writeFileSync(`../charlinfo-db/cours/${req.params.cours}/${val}`, req.files.contenu.data); // Sauvegarde le fichier
    }
    else {
        fs.writeFileSync(`../charlinfo-db/cours/${req.params.cours}/${val}.${extens}`, req.files.contenu.data); // Sauvegarde le fichier
    }
    ACTION.LogsUploadFichier(req.session.user.id, req);

    await res.redirect("/cours/afficher/" + req.params.cours);
}

/**
 * Traitement de création d'un cours
 */
exports.creationCours = async function (req, res) {

    await PERMISSION.gererPermission(req, res, "/", 4);

    let arr = await fs.readdirSync("../charlinfo-db/cours");

    let fin = [];
    for (let ob of arr) {
        if (!verifyIfNotInt(ob)) await fin.push(ob);
    }
    // Dans fin sera stocké les dossiers qui sont numérotés par des nombres
    let idCours = await getSeqFromIntegers(fin);
    let idFormation = await req.body.formation.split(":")[0];
    if (verifyIfNotInt(idFormation)) {
        logerror(`L'ID de la formation ne correpond pas au format "n: XXXXX" [CELA NE DEVRAIT JAMAIS ARRIVER]`);
        return await res.redirect("/");
    }
    idFormation = await parseInt(idFormation)

    let numFormations = await requete("SELECT id FROM Formation");

    let trouve = await trouver(numFormations, "id", idFormation);
    async function trouver(tab, modif, compare) {
        let trouve = false;
        for (let obj of tab) {
            if (obj[modif] === compare) {
                trouve = true;
                break;
            }
        }
        return trouve;
    }

    if (!trouve) {
        logerror(`L'ID de la formation n'existe pas [CELA NE DEVRAIT JAMAIS ARRIVER]`);
        return await res.redirect("/");
    }

    let duree = await requete("SELECT duree FROM formation WHERE id = $1", [idFormation]);
    if (req.body.semestre > duree[0].duree || req.body.semestre < 0) {
        logwarning(`Le cours est dans un semestre qui n'existe pas pour cette formation`);
        return await res.redirect("/");
    }

    requete("INSERT INTO Cours VALUES ($1::integer, $2, $3::integer, $4::integer, $5, $6, $7)", [
        idCours, req.body.nom, idFormation, req.body.semestre, req.body.desc, req.body.type, req.body.couleur
    ])

    if (req.body.profs !== "-----") {
        let prof = await req.body.profs.split(":")[0];
        if (verifyIfNotInt(prof)) {
            logerror(`L'ID du professeur ne correpond pas au format "n: XXXXX" [CELA NE DEVRAIT JAMAIS ARRIVER]`);
        }
        else {
            let numProfs = await requete("SELECT id FROM Professeur");
            let trouve = await trouver(numProfs, "id", parseInt(prof));
            if (!trouve) {
                logerror(`L'ID du professeur n'existe pas [CELA NE DEVRAIT JAMAIS ARRIVER]`);
            }
            else {
                requete("INSERT INTO Assurer_Cours VALUES ($1::integer, $2::integer)", [idCours, prof]);
            }
        }
    }

    ACTION.ajouterDossier(`../charlinfo-db/cours/${idCours}`)
    // ACTION.LogsGererCours(req.session.user.id, req.body.nom, "ajoutCours");
    await res.redirect('/cours/ajouter');
}


/**
 * Processus de modification d'un cours
 */
exports.modifierCours = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 4);

    let reqe = await requete("SELECT Count(*) FROM Cours WHERE id = $1::integer", [req.params.coursModif]);
    if (reqe[0].count == 0) {
        logerror("L'ID du cours qui est en modification n'existe pas [CELA NE DEVRAIT JAMAIS ARRIVER]");
        return await res.redirect("/cours/modifier");
    }
    await requete("UPDATE COURS SET type = $1, couleur = $2 where id = $3::integer", [req.body.type, req.body.couleur, req.params.coursModif]);
    // ACTION.LogsGererCours(req.session.user.id, req.body.nom, "modifCours");

    res.redirect("/cours/modifier");
}


/**
 * Processus de suppression d'un cours
 */
exports.supprimerCours = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 4);

    let reqe = await requete("SELECT Count(*) FROM Cours WHERE id = $1::integer", [req.params.coursSuppr]);
    if (reqe[0].count == 0) {
        logerror("L'ID du cours qui est en suppression n'existe pas [CELA NE DEVRAIT JAMAIS ARRIVER]");
        return await res.redirect("/cours/modifier");
    }
    ACTION.supprimerDossier("../charlinfo-db/cours/" + req.params.coursSuppr);
    await requete("DELETE FROM Fichier WHERE idCours = $1::integer", [req.params.coursSuppr])
    await requete("DELETE FROM Assurer_Cours WHERE idCours = $1::integer", [req.params.coursSuppr])
    requete("DELETE FROM Cours WHERE id = $1::integer", [req.params.coursSuppr])
    // ACTION.LogsGererCours(req.session.user.id, req.body.nom, "supprCours");

    res.redirect("/cours/modifier");
}


/**
 * Page pour traiter l'ajout d'un utilisateur
 */
exports.ajoutUtilisateur = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/users", 4);
    let reqe = await requete("SELECT id FROM Compte WHERE id = $1", [req.body.id]);
    if (reqe.length === 1) {
        req.session.userAjoute = `ERREUR : L'utilisateur ${req.body.id} est déjà dans la base de données`;
        return await res.redirect("/users");
    }
    requete("INSERT INTO Compte VALUES($1, $2, $3, $4, $5, $6, $7)", [
        req.body.id, null, null, null, 1, null, await DATE.dateISO()]
    );
    ACTION.LogsAjouterUtilisateur(req.session.user.id, req.body.id);
    req.session.userAjoute = `L'utilisateur ${req.body.id} a été ajouté !`;
    await res.redirect("/users");
}

/**
 * Traitement de l'ajout d'un Changelog de mise à jour
 */
exports.ajoutChangelog = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/changelog", 5);
    await requete("INSERT INTO Changelog VALUES ($1, $2)", [
        req.body.version, await DATE.dateISO()
    ]);
    let descs = await req.body.desc.split(">");
    for (let desc of descs) {
        await requete("INSERT INTO Description_changelog VALUES ($1, $2)", [req.body.version, desc]);
    }
    req.session.changelog = await requete_recuperer_version();
    req.session.changelog = req.session.changelog[0];
    await res.redirect("/changelog");
}