/**
 * Fonction qui charge une page
 * @param {Promise[]} promesses Tableau de promesses de requêtes SQL
 * @param {string[]} noms Tableau des noms associés aux promesses dans la page PUG
 * @param {object} autres Autres paramètres, sous forme d'objet 
 * @param {string} page Page à charger
 * @param {*} req req
 * @param {*} res res
 */
exports.charger = async (promesses = [], noms = [], autres = {}, page = "index.pug", req, res) => {
    let param = await (typeof(autres) !== "object") ? {} : autres;
    param.USER = req.session.user;
    param.VERSION = req.session.changelog;
    if (promesses.length !== 0){
        let values = await Promise.all(promesses);
        for(let i=0; i<noms.length ; i++) {
            if(values[i].length > 1) param[noms[i]] = await values[i];
            else param[noms[i]] = await values[i][0];
        }
    }
    res.render(`${page}`, param);
}