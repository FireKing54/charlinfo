const {Client} = require('pg');

const client = new Client(require("../database/.connect.json"));

client.connect();

/**
 * Fonction asynchrone qui retourne une promesse de résolution d'un requête SQL
 * @param {string} requete Requete SQL
 * @param {array} args (optionnel) Arguments de la requête
 * @returns {Promise} Promesse
 */
exports.requete = async function(requete, args) {
    let temp;
    if (args == undefined){
        temp = await client.query(requete);
    }
    else temp = await client.query(requete, args);
    return temp.rows;
}

async function requeteInterne(requete, args) {
    let temp;
    if (args == undefined){
        temp = await client.query(requete);
    }
    else temp = await client.query(requete, args);
    return temp.rows;
}

exports.requete_recuperer_version = async function() {
    return requeteInterne(
        "SELECT nom, date " +
        "FROM Changelog " +
        "ORDER BY date DESC " +
        "LIMIT 1"
    );
}

exports.requete_recuperer_changelog = async function() {
    return requeteInterne(
        "SELECT nom, date FROM changelog ORDER BY date DESC"
    );
}