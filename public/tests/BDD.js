const fs = require('fs');
const {Client} = require('pg');
const client = new Client(require("../database/.connect.json"));

const PATH = "public/database/upgrade";
const {logcolor} = require("../modules/color.js");

client.connect();

function extraire() {
    let fichiers;
    let res = [];
    try {
        fichiers = fs.readdirSync(PATH);
    }
    catch (err) {
        logcolor(`[ERREUR] le dossier situé sur ${PATH} n'a pas été trouvé.`, "rouge");
    }

    for(let i = 0 ; i < fichiers.length ; i++) {
        try {
            res.push(fs.readFileSync(`${PATH}/${fichiers[i]}`, 'utf8'));   
        }
        catch(err) {
            logcolor(`[ERREUR] dans le chargement du fichier ${fichiers[i]} du dossier ${PATH}.`, "rouge");
        }
    }
    let cop = [];
    for(let e of res) {
        cop.push(e.split("\n"))
    }
    res = [];
    for(let i = 0 ; i < cop.length ; i++) {
        res = res.concat(cop[i]);
    }
    return res;
}

function sortirRequete(ext) {
    let requetes = ext;
    requetes = requetes.join(" ");
    requetes = requetes.split(";");
    for(let i in requetes) requetes[i] += ';';
    let res = [];
    for(let el of requetes) {
        if (el !== ';') {
            res.push(el)
        }
    }
    return res;
}

function enleverCommentaires(arr) {
    let res = [];
    for(let req of arr) {
        if (!(req.charAt(0) === '-' && req.charAt(1) === '-')) {
            if ( req !== '') {
                res.push(req);
            }
        }
    }
    return res;
}

function executerRequete(element, index, end) {
    client.query(element)
    .then(() => {
        logcolor(`Requete ${index} réussie !`, "blue")
        if (end-1 == index) {
            logcolor("Terminé !", "green");
            process.exit(0);
        }
    })
    .catch(err => {
        logcolor(`-------------------------------------------------`, "red");
        logcolor(`Il y a eu une erreur dans la requête :\n${element}`, "red");
        logcolor(`${(err.detail) ? err.detail : err}`, "red");
        logcolor(`-------------------------------------------------`, "red");
        return "Erreur";
    })
}

async function executerRequetes(ext) {
    let end = [];
    for(let i in ext) {
        let reqs = await executerRequete(ext[i], i, ext.length)
        await end.push(reqs);
    }
}

async function viderDossiers() {
    viderCours();
    viderUsers();
}

async function viderCours() {
    const PATH = "../charlinfo-db"
    let cours = await fs.readdirSync(`${PATH}/cours`);
    for(let c of cours) {
        let fich;
        try {
            fich = await fs.readdirSync(`${PATH}/cours/${c}`);
            await fich.forEach(element => {
                fs.unlinkSync(`${PATH}/cours/${c}/${element}`);
            });
        }
        catch (err) {
            await fs.unlinkSync(`${PATH}/cours/${c}`)
        }
        await fs.rmdirSync(`${PATH}/cours/${c}`);
    }
}

async function viderUsers() {
    const PATH = "../charlinfo-db"
    let users = await fs.readdirSync(`${PATH}/users`);
    for(let u of users) {
        if (u !== "hashage" && u !== "users.json"){
            let fich;
            try {
                fich = await fs.readdirSync(`${PATH}/users/${u}`);
                await fich.forEach(element => {
                    fs.unlinkSync(`${PATH}/users/${u}/${element}`);
                });
            }
            catch (err) {
                await fs.unlinkSync(`${PATH}/users/${u}`);
            }
            await fs.rmdirSync(`${PATH}/users/${u}`);
        }
    }
}

let ext = extraire();
ext = enleverCommentaires(ext);
ext = sortirRequete(ext);
executerRequetes(ext);
viderDossiers();
