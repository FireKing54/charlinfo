// Chargement des middlewares
const express = require('express');
const pug = require('pug');
const fs = require('fs');
var fileUpload = require('express-fileupload')
var session = require('cookie-session');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Chargement d'Express
var app = express();

// Définit les dossiers "public" et "charlinfo-db" comme statiques (on peut charger du non HTML depuis)
app.use(express.static('public'));
app.use(express.static('../charlinfo-db'));

// Définit le secret de la session, pour l'initialisation
app.use(session({secret: "superSite"}));

// ---------------------------------------------------------------------------------------------------------------------

/**
 * On initialise le middleware pour l'upload
 */
app.use(fileUpload());

// Pages du site
app.get("/", (req, res) => pagePrincipale(req, res));
app.get("/charte", (req, res) => charte(req, res));
app.get("/deconnexion", (req, res) => deconnexion(req, res));
app.get("/cours", (req, res) => formation(req, res));
app.get("/cours/ajouter", (req, res) => pageAjoutCours(req, res));
app.get("/cours/modifier", (req, res) => pageModifCours(req, res));
app.get("/cours/afficher/:cours", (req, res) => afficherCours(req, res));
app.get("/cours/ajoutFichier/:cours", (req, res) => pageAjoutFichierCours(req, res));
app.get("/cours/:formation", (req, res) => semestre(req, res));
app.get("/cours/:cours/:fichSuppr/supprimer", (req, res) => suppressionFichier(req, res));
app.get("/cours/:formation/:semestre", (req, res) => modules(req, res));
app.get("/users", (req, res) => pageListeUtilisateurs(req, res));
app.get("/users/:user", (req, res) => afficherProfil(req, res));
app.get("/users/:user/bannir", (req, res) => bannir(req, res));
app.get("/users/:user/debannir", (req, res) => debannir(req, res));
app.get("/users/:user/retrograder", (req, res) => retrograder(req, res));
app.get("/users/:user/promouvoir", (req, res) => promouvoir(req, res));
app.get("/users/:user/supprimer", (req, res) => supprimerUtilisateur(req, res));
app.get("/users/:user/supprimer/:index", (req, res) => supprimerLog(req, res));
app.get("/changelog", (req, res) => pageAjoutChangelog(req, res));

// Pages de traitement
app.post('/connexion/', urlencodedParser, (req, res) => connexion(req, res));
app.post('/inscription/', urlencodedParser, (req, res) => inscription(req, res));
app.post("/cours/ajouter/ajout/", urlencodedParser, (req, res) => creationCours(req, res));
app.post("/cours/:cours/ajouter/", urlencodedParser, (req, res) => ajoutFichierCours(req, res));
app.post("/cours/modifier/:coursModif/", urlencodedParser, (req, res) => modifierCours(req, res));
app.post("/cours/supprimer/:coursSuppr/", urlencodedParser, (req, res) => supprimerCours(req, res));
app.post("/users/ajouter", urlencodedParser, (req, res) => ajoutUtilisateur(req, res));
app.post("/changelog/ajouter/", urlencodedParser, (req, res) => ajoutChangelog(req, res));

// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
// Fonctions de génération de pages

const {
    charte,
    pagePrincipale,
    deconnexion,
    formation,
    suppressionFichier,
    pageAjoutFichierCours,
    semestre,
    modules,
    pageAjoutCours,
    pageModifCours,
    pageListeUtilisateurs,
    afficherCours,
    afficherProfil,
    bannir,
    debannir,
    retrograder,
    promouvoir,
    supprimerUtilisateur,
    pageAjoutChangelog,
    supprimerLog
} = require("./public/pages/GET.js");

const {
    connexion,
    inscription,
    ajoutFichierCours,
    creationCours,
    modifierCours,
    supprimerCours,
    ajoutUtilisateur,
    ajoutChangelog
} = require("./public/pages/POST.js");

app.listen(8082);