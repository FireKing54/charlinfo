# Charlinfo

## INSTALLATION

### 0. Prérequis
* Installer git [https://git-scm.com/downloads]
* Installer nodejs [https://nodejs.org/en/] (Version LTS)
* Installer postgreSQL [https://www.postgresql.org/download/]

### 1. Setup
* Se placer dans le répertoire `charlinfo`
* Installer les dépendances node : `npm install` (Si l'opération boucle à l'infini au bout d'un moment, faire `CTRL+C`)
* Installer la base de données :
* ```bash
  sudo su postgres
  psql
  ```
* Créer la base de données : `CREATE DATABASE Charlinfo WITH ENCODING 'utf8'`;
* Changer le mot de passe postgres : `\password postgres`
* Copier coller les requêtes dans les fichiers `00X.sql` (sans le `000.sql`), dans le dossier `public/database/upgrade`
* [Les étapes à partir d'ici vont évoluer avec le sprochaines versions de Charlinfo] ...

### 2. Lancer le site
* Lancer une des neux commandes suivantes, à partir du répertoire `charlinfo`
* ```bash
  node index.js
  npm run start
  ```
* Récupérer le site à l'adresse `localhost:8082`


## TODO List

### Beta 1.4)
- Ajouter une base de données SQL fonctionnelle
- Fixer le problème dans le chemin des fichiers
- Fixer le problème du crash du site si on demande un cours qui n'existe pas 
- Ajouter une limite de taille de fichiers par grade
- Ajouter une interface pour montrer qu'il faut s'inscrire la première fois.
- Ajouter des statistiques sur les Logs (persmission >= 4)

### Beta 1.5)
- Ajouter un moyen d'éditer des fichiers (permission >= 3)
- Ajouter un moyen d'éditer un cours (description, profs, etc...)
- Faire en sorte d'améliorer graphiquement la page de profil

### Beta 1.6)
- Ajout d'un tri possible des fichiers (ordre alphabétique, date de sortie, grade)
- Ajout d'un système de recherche par mot-clé

### Beta 1.7)
- Ajout d'un upload de photo de profil
- Ajout d'une sorte de boîte de mail (+ système serveur qui va avec pour rendre ça simple)
- Ajout d'envoi d'avertissements

### Beta 1.8)
- Organiser le site (cours->Info->S2)
- Amélioration graphique de tout le site
- Ajout de media queries

### Beta 1.9)
- Ajout du système de notation des fichiers

### Beta 1.10)
- Correction de bugs
- Ajout de petites fonctionnalités de dernière minute
- Complétion de la charte
- Attendre un feedback

### 1.0
- Ajout dans le BDD de toute la promo